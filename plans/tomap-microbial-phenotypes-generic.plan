<?xml version="1.0" encoding="UTF-8"?><alvisnlp-plan id="tomap-on-phenotypes">
  
  <!-- ToMap on lemmas -->
  <tomap class="TomapProjector">
    <yateaFile output-feed="true">yatea/candidates.xml</yateaFile>
    <targetLayerName>phenotypes</targetLayerName>
    <conceptFeature>concept-id</conceptFeature>
    <explanationFeaturePrefix>explain_</explanationFeaturePrefix>
    <tomapClassifier empty-words="resources/stopwords_EN.ttg" whole-proxy-distance="false">&ontobiotope;-Phenotype.tomap</tomapClassifier>
    <lemmaKeys/>
    <subject feature="lemma" layer="words"/>
    <scoreFeature>score</scoreFeature>
  </tomap>

  <concept-names class="OBOMapper">
    <oboFiles>&ontobiotope;-Phenotype.obo</oboFiles>
    <idKeys/>
    <target>documents.sections.layer:phenotypes</target>
    <form>@concept-id</form>
    <nameFeature>concept-name</nameFeature>
    <pathFeature>concept-path</pathFeature>
  </concept-names>

  <!-- Repeat ToMap using "customized" lemmas -->
  <tomap-on-alternative-lemmas>

    <tomap class="TomapProjector">
      <yateaFile output-feed="true">yatea/candidates.xml</yateaFile>
      <targetLayerName>phenotypes2</targetLayerName>
      <conceptFeature>concept-id</conceptFeature>
      <explanationFeaturePrefix>explain_</explanationFeaturePrefix>
      <tomapClassifier empty-words="resources/stopwords_EN.ttg" whole-proxy-distance="false">&ontobiotope;-Phenotype.tomap</tomapClassifier>
      <lemmaKeys/>
      <subject feature="lemma2" layer="words"/>
      <scoreFeature>score</scoreFeature>
    </tomap>

    <concept-names class="OBOMapper">
      <oboFiles>&ontobiotope;-Phenotype.obo</oboFiles>
      <idKeys/>
      <target>documents.sections.layer:phenotypes2</target>
      <form>@concept-id</form>
      <nameFeature>concept-name</nameFeature>
      <pathFeature>concept-path</pathFeature>
    </concept-names>

    <!-- Add only those that are not already in layer 'phenotypes'-->
    <add-phenotypes class="Action">
      <target>documents.sections.layer:phenotypes2[not span:phenotypes]</target>
      <action>add:phenotypes</action>
      <addToLayer/>
    </add-phenotypes>

  </tomap-on-alternative-lemmas>

  <!-- Repeat ToMap without lemmaKeys option -->
  <tomap-no-lemmakeys>

    <tomap class="TomapProjector">
      <yateaFile output-feed="true">yatea/candidates.xml</yateaFile>
      <targetLayerName>phenotypes3</targetLayerName>
      <conceptFeature>concept-id</conceptFeature>
      <explanationFeaturePrefix>explain_</explanationFeaturePrefix>
      <tomapClassifier empty-words="resources/stopwords_EN.ttg" whole-proxy-distance="false">&ontobiotope;-Phenotype.tomap</tomapClassifier>
      <subject feature="lemma" layer="words"/>
      <scoreFeature>score</scoreFeature>
    </tomap>

    <concept-names class="OBOMapper">
      <oboFiles>&ontobiotope;-Phenotype.obo</oboFiles>
      <idKeys/>
      <target>documents.sections.layer:phenotypes3</target>
      <form>@concept-id</form>
      <nameFeature>concept-name</nameFeature>
      <pathFeature>concept-path</pathFeature>
    </concept-names>

    <!-- Add only those that are not already in layer 'phenotypes'-->
    <add-phenotypes class="Action">
      <target>documents.sections.layer:phenotypes3[not span:phenotypes]</target>
      <action>add:phenotypes</action>
      <addToLayer/>
    </add-phenotypes>

  </tomap-no-lemmakeys>


  <!-- Repeat ToMap on variants -->
  <tomap-on-variants>

    <tomap class="TomapProjector">
      <yateaFile output-feed="true">yatea-var/candidates.xml</yateaFile>
      <targetLayerName>phenotypes4</targetLayerName>
      <conceptFeature>concept-id</conceptFeature>
      <explanationFeaturePrefix>explain_</explanationFeaturePrefix>
      <tomapClassifier empty-words="resources/stopwords_EN.ttg" whole-proxy-distance="false">&ontobiotope;-Phenotype.tomap</tomapClassifier>
      <lemmaKeys/>
      <subject feature="variant" layer="words"/>
      <scoreFeature>score</scoreFeature>
    </tomap>

    <concept-names class="OBOMapper">
      <oboFiles>&ontobiotope;-Phenotype.obo</oboFiles>
      <idKeys/>
      <target>documents.sections.layer:phenotypes4</target>
      <form>@concept-id</form>
      <nameFeature>concept-name</nameFeature>
      <pathFeature>concept-path</pathFeature>
    </concept-names>

    <!-- Add only those that are not already in layer 'phenotypes'-->
    <add-phenotypes class="Action">
      <target>documents.sections.layer:phenotypes4[not span:phenotypes]</target>
      <action>add:phenotypes</action>
      <addToLayer/>
    </add-phenotypes>

  </tomap-on-variants>

  <!-- Repeat ToMap on words without lemmaKeys option -->
  <tomap-no-lemmakeys-word-form>

    <tomap class="TomapProjector">
      <yateaFile output-feed="true">yatea/candidates.xml</yateaFile>
      <targetLayerName>phenotypes5</targetLayerName>
      <conceptFeature>concept-id</conceptFeature>
      <explanationFeaturePrefix>explain_</explanationFeaturePrefix>
      <tomapClassifier empty-words="resources/stopwords_EN.ttg" whole-proxy-distance="false">&ontobiotope;-Phenotype.tomap</tomapClassifier>
      <subject feature="form" layer="words"/>
      <scoreFeature>score</scoreFeature>
    </tomap>

    <concept-names class="OBOMapper">
      <oboFiles>&ontobiotope;-Phenotype.obo</oboFiles>
      <idKeys/>
      <target>documents.sections.layer:phenotypes5</target>
      <form>@concept-id</form>
      <nameFeature>concept-name</nameFeature>
      <pathFeature>concept-path</pathFeature>
    </concept-names>

    <!-- Add only those that are not already in layer 'phenotypes'-->
    <add-phenotypes class="Action">
      <target>documents.sections.layer:phenotypes5[not span:phenotypes]</target>
      <action>add:phenotypes</action>
      <addToLayer/>
    </add-phenotypes>

  </tomap-no-lemmakeys-word-form>

  <!-- Postprocessing  -->
  <!-- Remove entities with certain characters -->
  <delete-char class="Action">
    <target>documents.sections.layer:phenotypes[@form =~ "^[~\\#\\*\\%\\u00D7=]" or @form =~ "\\#\\d+$" or @form =~ "\\-\\-" or (overlapping:units and start == overlapping:units{0}.start)]</target>
    <action>remove:phenotypes</action>
    <removeFromLayer/>
  </delete-char>

 <delete-unbalanced-parenthesis class="Action">
    <target>documents.sections.layer:phenotypes[@form =~ "^[^\\(]*\\([^\\)]+$"]</target>
    <action>remove:phenotypes</action>
    <removeFromLayer/>
  </delete-unbalanced-parenthesis>

  <!-- Projet BioYatea results -->
  <bioyatea-projection class="YateaTermsProjector">
    <targetLayerName>yateaTerms</targetLayerName>
    <yateaFile output-feed="yes">yatea/candidates.xml</yateaFile>
    <subject layer="words"/>
    <termLemma>lemma</termLemma>
  </bioyatea-projection>

  <!-- Generate lemmatized string for predicted phenotypes and for yatea terms-->
  <get-lemmas-for-predicted-phenotypes class="Action">
    <target>documents.sections.layer:phenotypes</target>
    <action>set:feat:lemma-string(str:join(overlapping:words[start != target.end],@lemma," "))</action>
    <setFeatures/>
  </get-lemmas-for-predicted-phenotypes>
  <get-lemmas-for-yateaTerms class="Action">
    <target>documents.sections.layer:yateaTerms</target>
    <action>set:feat:lemma-string(str:join(overlapping:words[start != target.end],@lemma," "))</action>
    <setFeatures/>
  </get-lemmas-for-yateaTerms>

  <syntactic-heads>
    <!-- Get head id and (lemmatized) string for predicted phenotypes -->
    <get-head-id class="Action">
      <target>documents.sections.layer:phenotypes</target>
      <action>set:feat:head-id(span:yateaTerms.@head)</action>
      <setFeatures/>
    </get-head-id>
    <get-full-head class="Action">
      <target>documents.sections.layer:phenotypes</target>
      <action>set:feat:full-head(overlapping:yateaTerms[@term-id == target.@head-id].@lemma-string)</action>
      <setFeatures/>
    </get-full-head>

    <!-- Get single-word syntactic head -->
    <get-mono-head class="Action">
      <target>documents.sections.layer:phenotypes</target>
      <action>set:feat:head(span:yateaTerms.@mono-head)</action>
      <setFeatures/>
    </get-mono-head>
    <get-mono-head2 class="Action">
      <target>documents.sections.layer:phenotypes[@head == ""]</target>
      <action>set:feat:head(@head-id)</action>
      <setFeatures/>
    </get-mono-head2>
    <get-mono-head-string class="Action">
      <target>documents.sections.layer:phenotypes</target>
      <action>set:feat:head-string(overlapping:yateaTerms[@term-id == target.@head].@lemma-string)</action>
      <setFeatures/>
    </get-mono-head-string>
  </syntactic-heads>

  <!-- Tag terms that fully include their mapped concept -->
  <inclusion class="Action">
    <target>documents.sections.layer:phenotypes</target>
    <action>set:feat:included(@full-head != "" and (@concept-name == @full-head or @explain_concept-synonym == @full-head))</action>
    <setFeatures/>
  </inclusion>

 <!-- Remove terms with blacklisted syntactic heads -->
  <delete-blacklisted-heads class="Action">
    <target>documents.sections.layer:phenotypes[@explain_significant-head in "/projet/maiage/save/textemig/projet-work/biotopes/resources/blacklisted-phenotype-heads.txt" and not (@concept-name =~ " " and (@lemma-string ?= @concept-name or @score == "1.0"))]</target>
    <action>remove:phenotypes</action>
    <removeFromLayer/>
  </delete-blacklisted-heads>

<!-- Choose a concept when multiple concepts have been mapped to a term -->

  <choose-candidate>
    <!-- 1. Keep only the highest scored concepts -->
    <keep-highest-score class="Action">
      <target>documents.sections.layer:phenotypes</target>
      <action>set:feat:highest(not span:phenotypes[@score &gt; target.@score])</action>
      <setFeatures/>
    </keep-highest-score>
    <!-- 2. Select candidates which (lemmatized) string matches a concept preferred name as opposed to those that match a concept synonym (this is for disambiguation of exact matches only)-->
    <select-one-candidate-1 class="Action">
      <target>documents.sections.layer:phenotypes[@highest == "true"]</target>
      <action>set:feat:selected1(@lemma-string == @concept-name or not span:phenotypes[@highest == "true" and @lemma-string == @concept-name])</action>
      <setFeatures/>
    </select-one-candidate-1>
    <!-- 3. Lexical inclusion : select in priority concepts that are included in the extracted habitat -->
    <select-included class="Action">
      <target>documents.sections.layer:phenotypes[@selected1 == "true"]</target>
      <action>set:feat:selected2(@included == "true" or not span:phenotypes[@selected1 == "true" and @included == "true"])</action>
      <setFeatures/>
    </select-included>
   <!-- 5. Select most generic candidate -->
  <select-generic class="Action">
    <target>documents.sections.layer:phenotypes[@selected2 == "true"]</target>
    <action>set:feat:selected3(not span:phenotypes[@selected2 == "true" and str:len(target.@concept-path) &gt; str:len(@concept-path)])</action>
    <setFeatures/>
  </select-generic>
    <!-- 4. Random : sort the candidates and choose the first one-->
    <temp-id class="Action">
      <target>documents.sections</target>
      <action>id:enumerate:tempid(sort:sval(layer:phenotypes[@selected3 == "true"],@concept-name))</action>
      <setFeatures/>
    </temp-id>
    <select-one-candidate class="Action">
      <target>documents.sections.layer:phenotypes[@selected3 == "true"]</target>
      <action>set:feat:selected(not span:phenotypes[@selected3 == "true" and target.@tempid &gt; @tempid])</action>
      <setFeatures/>
    </select-one-candidate>
  <!-- delete not selected -->
  <delete-not-selected class="Action">
    <target>documents.sections.layer:phenotypes[not @selected == "true"]</target>
    <action>remove:phenotypes</action>
    <removeFromLayer/>
  </delete-not-selected>
  </choose-candidate>

  <!-- Filtering of embedded terms -->

  <filter-embedded>
    <!-- remove terms nested in terms mapped with a more specific concept -->
    <remove-embedded-generic>
      <keep class="Action">
	<target>documents.sections.layer:phenotypes[@selected == "true"]</target>
	<action>set:feat:no-parent(not outside:phenotypes[@selected == "true" and @concept-path ?= target.@concept-id])</action>
	<setFeatures/>
      </keep>
      <delete class="Action">
	<target>documents.sections.layer:phenotypes[@no-parent == "false"]</target>
	<action>delete</action>
	<deleteElements/>
      </delete>
    </remove-embedded-generic>

    <!-- remove terms nested in terms with the same (significant) head -->
    <remove-embedded-in-term-with-same-significant-head>
      <keep class="Action">
	<target>documents.sections.layer:phenotypes[@selected == "true"]</target>
	<action>set:feat:diff-head(not outside:phenotypes[@selected == "true" and @explain_significant-head == target.@explain_significant-head and not @explain_significant-head == ""])</action>
	<setFeatures/>
      </keep>
      <delete class="Action">
	<target>documents.sections.layer:phenotypes[@diff-head == "false"]</target>
	<action>delete</action>
	<deleteElements/>
      </delete>
    </remove-embedded-in-term-with-same-significant-head>

    <!-- remove terms nested in terms with the same (mono-word) head -->
    <remove-embedded-in-term-with-same-head>
      <keep class="Action">
	<target>documents.sections.layer:phenotypes[@selected == "true"]</target>
	<action>set:feat:diff-head(not outside:phenotypes[@selected == "true" and @head == target.@head and not @head == ""])</action>
	<setFeatures/>
      </keep>
      <delete class="Action">
	<target>documents.sections.layer:phenotypes[@diff-head != "true"]</target>
	<action>delete</action>
	<deleteElements/>
      </delete>
    </remove-embedded-in-term-with-same-head>

  </filter-embedded>

  <!-- remove blacklisted terms -->
  <delete-blacklisted class="Action">
    <target>documents.sections.layer:phenotypes[@selected == "true" and @lemma-string in "/projet/maiage/save/textemig/projet-work/biotopes/resources/phenotype_blacklist.txt"]</target>
    <action>remove:phenotypes</action>
    <removeFromLayer/>
  </delete-blacklisted>

  <!-- remove single-word terms with certain POS -->
  <delete-POS-tags class="Action">
    <target>documents.sections.layer:phenotypes[@selected == "true" and span:words.@tt_pos =~ "(VVN|VVZ|VVD|VVP)"]</target>
    <action>remove:phenotypes</action>
    <removeFromLayer/>
  </delete-POS-tags>

  <!-- remove terms with scores under a certain threshold (0.15) -->
  <delete-low-score class="Action">
    <target>documents.sections.layer:phenotypes[@selected == "true" and 0.15 &gt; @score]</target>
    <action>remove:phenotypes</action>
    <removeFromLayer/>
  </delete-low-score>

  <!-- remove conjugating when term is not ending in "ing" -->
  <delete-conjugate class="Action">
    <target>documents.sections.layer:phenotypes[@selected == "true" and @concept-name == "conjugating" and not @form =~ "ing$"]</target>
    <action>remove:phenotypes</action>
    <removeFromLayer/>
  </delete-conjugate>

  <!-- Deal with abbreviations -->

  <get-concept-id-for-abbreviations class="Action">
    <target>documents.sections.layer:short-forms[not outside:words]</target>
    <action>set:feat:concept-id(str:join:'|'(sort:nsval(tuples:abbreviations:short-form.args:long-form.span:phenotypes[@selected == "true"],@concept-id),@concept-id)) | set:feat:score(str:join:'|'(sort:nsval(tuples:abbreviations:short-form.args:long-form.span:phenotypes[@selected == "true"],@concept-id), @score)) | set:feat:concept-name(str:join:'|'(sort:nsval(tuples:abbreviations:short-form.args:long-form.span:phenotypes[@selected == "true"],@concept-id),@concept-name))</action>
    <setFeatures/>
  </get-concept-id-for-abbreviations>
  <remove-abb class="Action">
    <target>documents.sections.layer:phenotypes[span:short-forms[not @concept-id == "" and not @concept-path =~ "OBT:000001/"]]</target>
    <action>remove:phenotypes</action>
    <removeFromLayer/>
  </remove-abb>
  <add-abb class="Action">
    <target>documents.sections.layer:short-forms[not @concept-id == "" and not @concept-path =~ "OBT:000001/"]</target>
    <action>set:feat:selected("true") | add:phenotypes</action>
    <addToLayer/>
    <setFeatures/>
  </add-abb>

  <!-- remove terms tag with generic concept "phenotype" -->

  <delete-generic-phenotypes class="Action">
    <target>documents.sections.layer:phenotypes[@selected == "true" and @concept-id == "OBT:000002"]</target>
    <action>remove:phenotypes</action>
    <removeFromLayer/>
  </delete-generic-phenotypes>

  <!-- add modifiers occurring before microorganisms -->
   <!-- <bacteria-modifier class="PatternMatcher"> -->
   <!--    <pattern> -->
   <!-- 	[not @form == "-" ] -->
   <!-- 	(modifier: [ @tt_pos == "JJ" and not overlapping:phenotypes and not overlapping:habitats and @lemma =~ "ic$"]+ ) -->
   <!-- 	[outside:microorganism]+ -->
   <!--    </pattern> -->
   <!--    <actions> -->
   <!-- 	<createAnnotation group="modifier" layer="phenotypes" features='ne-type="Phenotype",concept-id="MoPh:00000520"'/> -->
   <!--    </actions> -->
   <!--  </bacteria-modifier> -->

   <!-- molecule + ... rules -->
  <!--  <molecule-projection-on-lemmas class="OBOProjector"> -->
  <!--    <caseInsensitive/> -->
  <!--    <idFeature>molecule-id</idFeature> -->
  <!--    <nameFeature>molecule-name</nameFeature> -->
  <!--    <pathFeature>molecule-path</pathFeature> -->
  <!--    <oboFiles>&ontobiotope;-Molecule.obo</oboFiles> -->
  <!--    <subject feature="lemma" layer="words"/> -->
  <!--    <targetLayerName>molecules</targetLayerName> -->
  <!--  </molecule-projection-on-lemmas> -->

  <!--  <molecule-projection-on-words class="OBOProjector"> -->
  <!--    <caseInsensitive/> -->
  <!--    <idFeature>molecule-id</idFeature> -->
  <!--    <nameFeature>molecule-name</nameFeature> -->
  <!--    <pathFeature>molecule-path</pathFeature> -->
  <!--    <oboFiles>&ontobiotope;-Molecule.obo</oboFiles> -->
  <!--    <subject feature="form" layer="words"/> -->
  <!--    <targetLayerName>molecules2</targetLayerName> -->
  <!--  </molecule-projection-on-words> -->
  <!--  <add-mol class="Action"> -->
  <!--    <target>documents.sections.layer:molecules2[not overlapping:molecules]</target> -->
  <!--    <action>add:molecules</action> -->
  <!--    <addToLayer/> -->
  <!--  </add-mol> -->

  <!--  <antibiotics-rule class="PatternMatcher"> -->
  <!--     <pattern> -->
  <!--  	[outside:molecules[@molecule-path =~ "/EC:0000272"] and not outside:phenotypes[@concept-path =~ "/MoPh:00001314/"]]+ -->
  <!-- 	[@form == "-"]? -->
  <!-- 	[@lemma =~ "^resistan"] -->
  <!--     </pattern> -->
  <!--     <actions> -->
  <!--  	<createAnnotation features="ne-type=&quot;Phenotype&quot;,concept-id=&quot;MoPh:00001314&quot;" layer="molecule-phenotypes"/> -->
  <!--     </actions> -->
  <!--   </antibiotics-rule> -->
  <!--  <antibiotics-rule-2 class="PatternMatcher"> -->
  <!--     <pattern> -->
  <!--  	[outside:molecules[@molecule-path =~ "/EC:0000272"] and not outside:phenotypes[@concept-path =~ "/EC:0000357/"]]+ -->
  <!-- 	[@form == "-"]? -->
  <!-- 	[@lemma =~ "^sensitiv"] -->
  <!--     </pattern> -->
  <!--     <actions> -->
  <!--  	<createAnnotation features="ne-type=&quot;Phenotype&quot;,concept-id=&quot;EC:0000357&quot;" layer="molecule-phenotypes"/> -->
  <!--     </actions> -->
  <!--   </antibiotics-rule-2> -->
  <!--  <antibiotics-rule-3 class="PatternMatcher"> -->
  <!--     <pattern> -->
  <!--  	[outside:molecules[@molecule-path =~ "/EC:0000272"] and not outside:phenotypes[@concept-path =~ "/EC:0000356/"]]+ -->
  <!-- 	[@form == "-"]? -->
  <!-- 	[@lemma =~ "^toleran"] -->
  <!--     </pattern> -->
  <!--     <actions> -->
  <!--  	<createAnnotation features="ne-type=&quot;Phenotype&quot;,concept-id=&quot;EC:0000356&quot;" layer="molecule-phenotypes"/> -->
  <!--     </actions> -->
  <!--   </antibiotics-rule-3> -->

  <!-- <oxidation-rule class="PatternMatcher"> -->
  <!--     <pattern> -->
  <!--  	[outside:molecules and not outside:phenotypes[@concept-path =~ "/MoPh:00000462/"]]+ -->
  <!-- 	[@form == "-"]? -->
  <!-- 	[str:lower(@form) == "oxidizing"] -->
  <!--     </pattern> -->
  <!--     <actions> -->
  <!--  	<createAnnotation features="ne-type=&quot;Phenotype&quot;,concept-id=&quot;MoPh:00000462&quot;" layer="molecule-phenotypes"/> -->
  <!--     </actions> -->
  <!--   </oxidation-rule> -->

  <!--  <remove-pheno-spanning-mol-pheno class="Action"> -->
  <!--    <target>documents.sections.layer:phenotypes[span:molecule-phenotypes]</target> -->
  <!--    <action>remove:phenotypes</action> -->
  <!--    <removeFromLayer/> -->
  <!--  </remove-pheno-spanning-mol-pheno> -->
  <!--  <add-mol-pheno class="Action"> -->
  <!--    <target>documents.sections.layer:molecule-phenotypes</target> -->
  <!--    <action>add:phenotypes</action> -->
  <!--    <addToLayer/> -->
  <!--  </add-mol-pheno> -->

<!-- Add concept-path in case some are missing -->
    <concept-path class="OBOMapper">
      <oboFiles>&ontobiotope;-Phenotype.obo</oboFiles>
      <idKeys/>
      <target>documents.sections.layer:phenotypes</target>
      <form>@concept-id</form>
      <nameFeature>concept-name</nameFeature>
      <pathFeature>concept-path</pathFeature>
    </concept-path>


</alvisnlp-plan>
